package sample;

import javafx.scene.control.TextArea;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class TextBox {
    static TextArea textArea =new TextArea();
    public static GridPane show(){
        textArea.setMaxHeight(Double.MAX_VALUE);
        textArea.setMaxWidth(Double.MAX_VALUE);

        GridPane gridPane = new GridPane();
        gridPane.setVgrow(textArea, Priority.ALWAYS);
        gridPane.setHgrow(textArea, Priority.ALWAYS);

        gridPane.getChildren().add(textArea);

        return gridPane;
    }
    static TextArea getTextArea()
    {
        return textArea;
    }
}
