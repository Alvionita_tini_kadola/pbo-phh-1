package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static sample.TextBox.getTextArea;

public class MenuChoose {
    public static BorderPane show (Stage window) {
        BorderPane layout;
        FileChooser fileChooser = new FileChooser();

        Menu fileMenu = new Menu("File");
        MenuItem newFile = new MenuItem("New");
        newFile.setOnAction((ActionEvent event) ->{
            TextBox.textArea.clear();
    });
        fileMenu.getItems().add(newFile);

        MenuItem openFile = new MenuItem("Open");
        openFile.setOnAction(e -> {
            fileChooser.setTitle("Open");
            File file = fileChooser.showOpenDialog(window);
            FileChooser.ExtensionFilter txtFilter = new FileChooser.ExtensionFilter("txt",".txt");
            fileChooser.getExtensionFilters().add(txtFilter);
            try{
                FileInputStream in = new FileInputStream(file);
                int c;
                TextArea textArea = getTextArea();
                textArea.clear();
                while((c = in.read())!=-1){
                    textArea.appendText(String.valueOf((char) c));
                }
            }catch (Exception e1){
             }
        });
        fileMenu.getItems().add(openFile);

        MenuItem saveFile = new MenuItem("Save");
        saveFile.setOnAction(e -> {
            String newLine = System.getProperty("line.separator");
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Text Files", ".txt"));
            File file = fileChooser.showSaveDialog(window);
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    String text = getTextArea().getText();
                        for (int i = 0; i < text.length(); i++) {
                            out.write(text.charAt(i));
                            out.write(newLine.charAt(0));
                        }
                    } catch (FileNotFoundException e1) {
                } catch (Exception e1) {
                        e1.printStackTrace();
                }
        });
        fileMenu.getItems().add(saveFile);

        fileMenu.getItems().add(new SeparatorMenuItem());

        MenuItem exitFile = new MenuItem("Exit");
        exitFile.setOnAction(e -> window.close());
        fileMenu.getItems().add(exitFile);

        fileMenu.getItems().add(new SeparatorMenuItem());

        Menu editMenu = new Menu("Edit");
        MenuItem cutFile = new MenuItem("Cut");
        editMenu.getItems().add(cutFile);

        MenuItem copyFile = new MenuItem("Copy");
        editMenu.getItems().add(copyFile);

        MenuItem pasteFile = new MenuItem("Paste");
        editMenu.getItems().add(pasteFile);

        Menu helpMenu = new Menu("Help");
        MenuItem aboutFile = new MenuItem("About");
        helpMenu.getItems().add(aboutFile);

        MenuItem versionFile = new MenuItem("Version");
        helpMenu.getItems().add(versionFile);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, editMenu, helpMenu);

        layout = new BorderPane();
        layout.setTop(menuBar);

        return layout;

    }
}
