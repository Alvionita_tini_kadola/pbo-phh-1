package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
       primaryStage.setTitle("Notepad");
       //primaryStage.getIcons(.add(new Image(getClass(.getResourceAsStream("Notepad.png")));
       primaryStage.setMinHeight(500);
       primaryStage.setMinWidth(500);

       //BorderPane
        MenuChoose menuChoose = new MenuChoose();
        BorderPane layout1= menuChoose.show(primaryStage);

        //TextArea
        TextBox textBox = new TextBox();
        GridPane layout2 = textBox.show();


        VBox vbox= new VBox();
        vbox.getChildren().addAll(layout1, layout2);
        vbox.setVgrow(layout2, Priority.ALWAYS);

        Scene scene =new Scene(vbox);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
